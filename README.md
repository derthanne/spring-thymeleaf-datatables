# Spring Boot Component for jQuery DataTables

> For information about jQuery DataTables see [datatables.net](https://datatables.net/)

This repository contains the [component](component) itself and a [sample](sample) application.
See their README.md files for details.

## Quick Start

1. Go to component
1. mvn install
1. Go to sample
1. mvn spring-boot:run
1. Go to http://localhost:8080 in your browser