package de.mthsoft.datatables;

import de.mthsoft.datatables.model.SortDirection;
import de.mthsoft.datatables.web.DataTablePage;
import de.mthsoft.datatables.web.DataTableRequest;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.comparator.ComparableComparator;
import org.springframework.util.comparator.NullSafeComparator;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

@Component
class MockService {

    private static final int MIN_SEARCH_LENGTH = 2;

    private static final int ELEMENTS_IN_MEMORY = 10000;

    private static Comparator<String> INNER_COMPARATOR = new NullSafeComparator<>( new ComparableComparator<String>(), true );

    private ArrayList<Entity> entities = new ArrayList<>();

    private Map<String,List<SubEntity>> subentities = new HashMap<>();

    @PostConstruct
    public void init() {
        for( int i = 0; i < ELEMENTS_IN_MEMORY; i++ ) {
            var b = !(i % 7 == 0);
            var identifier = UUID.randomUUID().toString();
            var string1 = RandomStringUtils.randomAlphabetic( 10 );
            var string2 = RandomStringUtils.randomAlphabetic( 12 );
            var string3 = RandomStringUtils.randomAlphabetic( 8 );
            var string4 = RandomStringUtils.randomAlphabetic( 10 );
            var date = Calendar.getInstance();
            if( i % 2 == 0 ) {
                date.add( Calendar.MONTH, -1 );
            }
            if( i % 3 == 0 ) {
                date.add( Calendar.YEAR, -1 );
            }
            if( i % 4 == 0 ) {
                date.add( Calendar.MONTH, -1 );
            }
            if( i % 5 == 0 ) {
                date.add( Calendar.DATE, -1 );
            }
            if( i % 6 == 0 ) {
                date.add( Calendar.MONTH, -1 );
            }
            if( i % 7 == 0 ) {
                date.add( Calendar.DATE, -1 );
            }
            if( i % 8 == 0 ) {
                date.add( Calendar.MONTH, -1 );
            }
            if( i % 9 == 0 ) {
                date.add( Calendar.YEAR, -1 );
            }
            if( i % 10 == 0 ) {
                date.add( Calendar.DATE, -1 );
            }
            entities.add( new Entity( identifier, b, string1, string2, string3, string4, date.getTime() ) );
        }
    }

    public DataTablePage<Entity> findEntities(DataTableRequest<Entity> request ) {
        var start = request.getStart();
        if( start < 0 || start >= entities.size() ) {
            return DataTablePage.empty();
        }
        var length = 0;
        if( start + request.getLength() <= entities.size() ) {
            length = request.getLength();
        } else {
            length = entities.size() - start;
        }
        var filtered = sort( request, findEntities( request, this.entities ) );
        if( start >= filtered.size() ) {
            return DataTablePage.empty();
        }
        var end = start + length;
        if( end >= filtered.size() ) {
            end = filtered.size();
        }
        var page = new PageImpl<>(filtered.subList(start, end), Pageable.unpaged(), entities.size());
        return DataTablePage.build( page, filtered.size(), entities.size() );
    }

    public DataTablePage<SubEntity> findSubEntities( String identifier ) {
        List<SubEntity> list = null;
        if( subentities.containsKey( identifier ) ) {
            list = subentities.get( identifier );
        } else {
            list = new ArrayList<SubEntity>();
            for( int i = 0; i < RandomUtils.nextInt(3,10); i++ ) {
                list.add( new SubEntity( identifier, i) );
            }
            subentities.put( identifier, list );
        }
        return DataTablePage.fromList( list );
    }

    private ArrayList<Entity> findEntities(DataTableRequest<Entity> request, ArrayList<Entity> all ) {
        if( request.getSearchValue().length() < MIN_SEARCH_LENGTH ) {
            return all;
        }
        //We know that only String1 and String3 are searchable
        var list = all.stream().filter( entity -> {
            return StringUtils.startsWithIgnoreCase( entity.getString1(), request.getSearchValue() ) || StringUtils.startsWithIgnoreCase( entity.getString3(), request.getSearchValue() );
        }).collect( Collectors.toList() );
        if( list instanceof ArrayList ) {
            return (ArrayList<Entity>) list;
        } else {
            return new ArrayList<>( list );
        }
    }

    private ArrayList<Entity> sort( DataTableRequest<Entity> request, ArrayList<Entity> unsorted ) {
        if( !request.isSorted() ) {
            return unsorted;
        }
        var list = Utils.deepCopy( unsorted );
        var method = Utils.getPropertyReader( Entity.class, request.getSortColumn() );
        var comparator = new Comparator<Entity>() {

            @Override
            public int compare(Entity o1, Entity o2) {
                var oo1 = ReflectionUtils.invokeMethod( method, o1 );
                var oo2 = ReflectionUtils.invokeMethod( method, o2 );
                var i = INNER_COMPARATOR.compare(
                        ( oo1 == null ) ? (String) null : oo1.toString(),
                        ( oo2 == null ) ? (String) null : oo2.toString()
                );
                if(SortDirection.DESC == request.getSortDirection() ) {
                    return i * -1;
                }
                return i;
            }
        };
        list.sort( new NullSafeComparator<>( comparator, true ));
        return list;
    }

}
