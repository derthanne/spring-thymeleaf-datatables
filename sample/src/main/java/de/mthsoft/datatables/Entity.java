package de.mthsoft.datatables;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Entity implements Serializable {

    private final String identifier;

    private final boolean bool;

    private final String string1;

    private final String string2;

    private final String string3;

    private final String string4;

    private final Date date;

    Entity( String identifier, boolean bool, String string1, String string2, String string3, String string4, Date date ) {
        this.identifier = identifier;
        this.bool = bool;
        this.string1 = string1;
        this.string2 = string2;
        this.string3 = string3;
        this.string4 = string4;
        this.date = date;
    }

    public String getIdentifier() {
        return identifier;
    }

    public boolean isBool() {
        return bool;
    }

    public String getString1() {
        return string1;
    }

    public String getString2() {
        return string2;
    }

    public String getString3() {
        return string3;
    }

    public String getString4() {
        return string4;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entity entity = (Entity) o;
        return identifier.equals(entity.identifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }

    @Override
    public String toString() {
        return "Entity{" +
                "identifier='" + identifier + '\'' +
                ", bool=" + bool +
                ", string1='" + string1 + '\'' +
                ", string2='" + string2 + '\'' +
                ", string3='" + string3 + '\'' +
                ", string4='" + string4 + '\'' +
                ", date=" + date +
                '}';
    }
}
