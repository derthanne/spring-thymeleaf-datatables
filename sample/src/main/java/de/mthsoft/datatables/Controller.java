package de.mthsoft.datatables;

import de.mthsoft.datatables.model.DataTableColumnDefinition;
import de.mthsoft.datatables.model.DataTableDefinition;
import de.mthsoft.datatables.web.DataTableComponent;
import de.mthsoft.datatables.web.DataTableRequest;
import de.mthsoft.datatables.web.DataTableResponse;
import de.mthsoft.datatables.web.MessageSourceDataTableTranslationProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.Locale;

@org.springframework.stereotype.Controller
class Controller {

    static final String XHTTP_REQUEST_HEADER = "X-Requested-With";

    private static final Logger LOG = LoggerFactory.getLogger( Controller.class );

    private final MockService service;

    private final DataTableDefinition<Entity> entitiesDefinition;

    private final DataTableDefinition<SubEntity> subEntitiesDefinition;

    private final SpringTemplateEngine templateEngine;

    private final MessageSource messageSource;

    Controller(@Autowired MockService service,
               @Autowired DataTableDefinition<Entity> entitiesDefinition,
               @Autowired DataTableDefinition<SubEntity> subEntitiesDefinition,
               @Autowired SpringTemplateEngine templateEngine,
               @Autowired MessageSource messageSource ) {
        this.service = service;
        this.entitiesDefinition = entitiesDefinition;
        this.subEntitiesDefinition = subEntitiesDefinition;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    @GetMapping( value = "/", produces = MimeTypeUtils.TEXT_HTML_VALUE )
    public ModelAndView index(Locale locale ) {
        ModelAndView mav = new ModelAndView( "index" );
        DataTableComponent<Entity> component = new DataTableComponent<>(
                null,
                entitiesDefinition,
                new DataTableTranslationProviderImpl(messageSource),
                locale );
        mav.addObject( "dataTable", component );
        return mav;
    }

    @PostMapping( value = Application.FETCH_ENTITIES_URI, produces = MimeTypeUtils.APPLICATION_JSON_VALUE, headers = XHTTP_REQUEST_HEADER )
    public ResponseEntity<DataTableResponse<Entity>> retrieveEntities(@RequestParam MultiValueMap<String, Object> body  ) {
        var request = new DataTableRequest<>(body, entitiesDefinition);
        try {
            var page = service.findEntities( request );
            return ResponseEntity.ok( DataTableResponse.fromResult( request, page ) );
        } catch ( Exception e ) {
            LOG.error( "Error retrieving entities!", e );
            return ResponseEntity.internalServerError().body( DataTableResponse.fromException( request, e ) );
        }
    }

    @PostMapping( value = Application.FETCH_SUBENTITIES_URI, produces = MimeTypeUtils.APPLICATION_JSON_VALUE, headers = XHTTP_REQUEST_HEADER )
    public ResponseEntity<DataTableResponse<SubEntity>> retrieveSubEntities(
            @PathVariable( name = "id") String id,
            @RequestParam MultiValueMap<String, Object> body ) {
        var request = new DataTableRequest<>(body, subEntitiesDefinition);
        try {
            var page = service.findSubEntities( id );
            return ResponseEntity.ok( DataTableResponse.fromResult( request, page ) );
        } catch ( Exception e ) {
            LOG.error( "Error retrieving subentities for entity " + id + "!", e );
            return ResponseEntity.internalServerError().body( DataTableResponse.fromException( request, e ) );
        }
    }

    static class DataTableTranslationProviderImpl extends MessageSourceDataTableTranslationProvider {

        DataTableTranslationProviderImpl(MessageSource ms) {
            super( ms );
        }

        @Nullable
        @Override
        protected String getInfoKey() {
            return null;
        }

        @Nullable
        @Override
        protected String getSearchKey() {
            return "demo.table.search";
        }

        @NotNull
        @Override
        protected String getColumnLabelKey(@NotNull DataTableDefinition<?> table, @NotNull DataTableColumnDefinition<?> column) {
            return "demo.table." + table.getName() + ".column." + column.getColumnName() + ".label";
        }

    }

}
