package de.mthsoft.datatables;

import de.mthsoft.datatables.model.DataTableDefinition;
import de.mthsoft.datatables.model.DataTableFeatures;
import de.mthsoft.datatables.model.DataTableOptions;
import de.mthsoft.datatables.model.SortDirection;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Locale;

@EnableAutoConfiguration
@ComponentScan( basePackageClasses = Application.class )
public class Application implements WebMvcConfigurer {

    public static final String BEAN_NAME_DEFINITION_ENTITIES = "entitiesDefinition";

    public static final String BEAN_NAME_DEFINITION_SUBENTITIES = "subEntitiesDefinition";

    static final String FETCH_ENTITIES_URI = "/entities";

    static final String FETCH_SUBENTITIES_URI = "/details/{id}";

    @Bean
    MessageSource messageSource() {
        var source = new ResourceBundleMessageSource();
        source.setDefaultLocale(Locale.getDefault() );
        source.setBasename( "messages" );
        return source;
    }

    @Bean( BEAN_NAME_DEFINITION_SUBENTITIES )
    DataTableDefinition<SubEntity> subEntityDataTableDefinition() throws Exception {
        return new DataTableDefinition.Builder<>("subentities", SubEntity.class, FETCH_SUBENTITIES_URI.replace( "{id}", "" ) )
                .withFeatures( DataTableFeatures.withAllDisabled() )
                .addColumn( "identifier").asIdentifier().asInvisible().withDefaultSortDirection( SortDirection.ASC ).withRender("return '';").endColumn()
                .addColumn( "value1" ).endColumn()
                .addColumn( "value2" ).endColumn()
                .addColumn( "value3" ).endColumn()
                .build();
    }

    @Bean( BEAN_NAME_DEFINITION_ENTITIES )
    DataTableDefinition<Entity> entityDataTableDefinition( DataTableDefinition<SubEntity> subTable ) throws Exception {
        var features = DataTableFeatures.withDefaults();
        features.setLengthChange( true );
        return new DataTableDefinition.Builder<>("entities", Entity.class, FETCH_ENTITIES_URI )
            .withOptions( DataTableOptions.withPagingType(DataTableOptions.PagingType.FULL_NUMBERS) )
            .withFeatures( features )
            .addColumn( "bool" ).withWidth( 1 ).withChildTable( subTable ).withRender( "return formatBooleanColumn( data );" ).endColumn()
            .addColumn( "string1" ).withDefaultSortDirection( SortDirection.ASC ).asSearchable().endColumn()
            .addColumn( "string2" ).asSortable().endColumn()
            .addColumn( "string3" ).asSearchable().endColumn()
            .addColumn( "string4" ).endColumn()
            .addColumn( "date" ).asSortable().withRender( "return formatDateColumnValue( data );" ).endColumn()
            //.addColumn( "identifier" ).asIdentifier().withChildTable( subTable ).endColumn()
            .addColumn( "identifier" ).asIdentifier().withWidth(1).withRender("return renderIdentifierColumn( data );").endColumn()
        .build();
    }

    public static void main( String[] args ) {
        SpringApplication.run( Application.class, args );
    }

}
