package de.mthsoft.datatables;

import java.io.Serializable;
import java.util.Objects;

public class SubEntity implements Serializable {

    private final String identifier;

    private final String value1;

    private final String value2;

    private final String value3;

    SubEntity(String identifier, int itemNo) {
        this.identifier = identifier;
        this.value1 = "value" + itemNo + "1";
        this.value2 = "value" + itemNo + "2";
        this.value3 = "value" + itemNo + "3";
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getValue1() {
        return value1;
    }

    public String getValue2() {
        return value2;
    }

    public String getValue3() {
        return value3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubEntity subEntity = (SubEntity) o;
        return identifier.equals(subEntity.identifier) && value1.equals(subEntity.value1) && value2.equals(subEntity.value2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier, value1, value2);
    }

    @Override
    public String toString() {
        return "SubEntity{" +
                "identifier='" + identifier + '\'' +
                ", value1='" + value1 + '\'' +
                ", value2='" + value2 + '\'' +
                ", value3='" + value3 + '\'' +
                '}';
    }
}
