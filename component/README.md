# Spring Boot Component for jQuery DataTables

[[_TOC_]]

## Introduction

This component tries to make a jQuery DataTable (just named DataTable in the following document) completely configurable from a Spring Boot WebMVC application.
It is also capable of using one level of subtables.

Basic classes to handle backend requirements of such a component are contained as well.

> For information about jQuery DataTables see [datatables.net](https://datatables.net/)
> > Please note that not all options or parameters of the jQuery component are currently implemented in this component, <br>
but if features are missing they can easily be added.

## Defining a DataTable

To define a DataTable a `DataTableDefinition` must be created using the `DataTableDefinition.Builder` class. Using the builder, columns can be added to the DataTable. 
Creating a column in the builder results in the creation of a `DataTableColumnDefinition.Builder` object that creates a `DataTableColumnDefinition` object.
Each column must be connected to a Java property.

*__Example:__*
```java
// Creates a DataTable named sampleTable for the Sample class, 
// that contains (at least) the properties sample1 and sample2.
// Data is loaded from /sample.json endpoint.
// The first column is the identifier and contains a custom renderer,
// the second column is sortable and searchable and rendered with the value (default).
DataTableDefinition<Sample> def = new DataTableDefinition.Builder<>( "sampleTable", Sample.class, "/sample.json" )
    .withOptions( DataTableOptions.withDefaults() )
    .withFeatures( DataTableFeatures.withDefaults() )
    .addColum("sample1").asIdentifier().withRender("return renderIdentifierColumn( data );").endColumn()
    .addColumn("sample2").asDefaultSortColumn().asSearchable().endColumn()
    .build();
```
> See the source code documentation for details.

### Defining a subtable

To define a subtable for your DataTable simply create another `DataTableDefinition` and add it to the table. 
> Please note that the parent table's identifier will be added to the load url. So `/sub/` will become `/sub/$id`.
> Also it is recommended to disable all features and do not use any options on this table.

*__Example__*
```java
// Creates a DataTable with one column
DataTableDefinition<Sub> sub = new DataTableDefinition.Builder<>( "subTable", Sample.class, "/sub/" )
    .withFeatures( DataTableFeatures.withAllDisabled() )
    .addColumn( "id" ).asIdentifier().withDefaultSortDirection( SortDirection.ASC ).endColumn()
    .build();

//Subtable added to property sample3
DataTableDefinition<Sample> def = [ ... ]
    .addColumn("sample3").withChildTable( sub ).endColumn()
```

## Using the definition with Spring Web MVC

### Setting up the model

To create a DataTable within an HTML page a `DataTableComponent` needs to be added to the model of a view.
The `DataTableComponent` uses an existing `DataTableDefinition` for the table definition and a `DataTableTranslationProvider` for internationalisation purposes.
It also needs the value of the table's HTML id attribute and the URL from which data can be retrieved.

*__Example:__*
```java
DataTableDefinition<Sample> definition = getDataTableDefinition();
//Get the DataTableTranslationProvider
DataTableTranslationProvider provider = obtainTranslationProvider();
//Create the component
DataTableComponent<Sample> component = new DataTableComponent<>( definition, provider);
//Add component named datatable to the ModelAndView
ModelAndView mav = new ModelAndView( "index" );
mav.addObject("datatable", component);
```
> See the source code documentation for details.

### Using the table in Thymeleaf templates

The component comes with ready-to-use Thymeleaf fragments for styles and scripts.
These can be added using the `datatable` prefix.

```html
<!-- datatable component styles --->
<link th:replace="datatable :: datatable_styles"/>
<!-- datatable component scripts --->
<script th:replace="datatable :: datatable_scripts"></script>
```

Instead of creating the `table` element yourself, another fragment can be used to do this.

```html
<table th:id="${dataTable.definition.name}" class="table table-striped table-bordered table-hover">
    <thead th:replace="datatable :: datatable_head"></thead>
</table>
```

All that remains is to write your custom render functions and call the `buildJavaScript' render function of the model object.

```html
<script type="text/javascript">
function renderIdentifierColumn( data ) {
    return "<button onclick=\"alert('Identifier " + data + "')\">...</button>";
}
</script>
<script type="text/javascript" th:utext="${dataTable.buildJavaScript()}"></script>
```


### Retrieving data with a controller

To retrieve data the request body needs to be converted into a `DataTableRequest`. 
The obtained data needs to be returned as a `DataTableResponse` object to fit the specification.

For ease of use `DataTablePage` is recommended. The interface provides convenient factory methods to convert `List` objects or Spring Data `Page` objects.

*__Example__*
```java
@PostMapping( value = "/sample.json", produces = "application/json", headers = "X-Requested-With" )
public ResponseEntity<DataTableResponse<Sample>> retrieveTable( @RequestParam MultiValueMap<String, Object> body ) {
    DataTableDefinition<Sample> definition = getDataTableDefinition();
    var request = new DataTableRequest<>(body, definition);
    try {
        //We expect getService() to provide an appropriate method
        DataTablePage<Sample> page = getService().search( request );
        //Handle the result
        return ResponseEntity.ok( 
            DataTableResponse.fromResult(request, page ) 
        );
    } catch ( Exception e ) {
        //Handle the error
        return ResponseEntity.internalServerError().body( 
            DataTableResponse.fromException( request, e ) 
        );
    }
}

@PostMapping( value = "/sub/{id}", produces = "application/json", headers = "X-Requested-With" )
public ResponseEntity<DataTableResponse<Sub>> retrieveSubTable( 
    @PathVariable( name = "id") String id,
    @RequestParam MultiValueMap<String, Object> body ) {
    DataTableDefinition<Sub> definition = getSubDataTableDefinition();
    var request = new DataTableRequest<>(body, definition);
    try {
        [...] /* See above */
    } catch ( Exception e ) {
        [...] /* See above */
    }    
}
```

