package de.mthsoft.datatables.web

import de.mthsoft.datatables.model.DataTableColumnDefinition
import de.mthsoft.datatables.model.DataTableDefinition
import de.mthsoft.datatables.model.DataTableFeatures
import de.mthsoft.datatables.model.DataTableOptions
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.StaticMessageSource
import org.thymeleaf.spring5.SpringTemplateEngine
import java.util.*

@SpringBootTest( classes = [ DataTableJavaScriptBuilderTest.Config::class ])
class DataTableJavaScriptBuilderTest {

    @Autowired
    private lateinit var templateEngine: SpringTemplateEngine;

    @Test
    fun testDefault() {
        val locale = Locale.getDefault();
        val messageSource = StaticMessageSource();
        messageSource.addMessage( "info", locale, "info" );
        messageSource.addMessage( "search", locale, "search" );
        messageSource.addMessage( "column", locale, "column" );
        val provider = object : MessageSourceDataTableTranslationProvider( messageSource ) {
            override val infoKey: String
                get() = "info"
            override val searchKey: String
                get() = "search"

            override fun getKeyForColumnLabel(table: DataTableDefinition<*>, column: DataTableColumnDefinition<*>) = "column";

        };
        val definition = DataTableDefinition.Builder( Sample::class.java.simpleName, Sample::class.java, "/test.json" )
            .withOptions(DataTableOptions.withPagingType(DataTableOptions.PagingType.FULL_NUMBERS))
            .withFeatures(DataTableFeatures.withDefaults())
            .addColumn( "column1").asIdentifier().asDefaultSortColumn().withWidth(1).withRender("alert(\"hello\");").endColumn()
            .build();
        val component = Mockito.mock( DataTableComponent::class.java );
        Mockito.`when`( component.locale ).thenReturn( locale );
        Mockito.`when`( component.definition ).thenReturn( definition );
        Mockito.`when`( component.translationProvider ).thenReturn( provider );
        val str = DataTableJavaScriptBuilder.create( component, templateEngine ).build();
        Assertions.assertTrue(str.isNotEmpty());
    }

    @Configuration
    @EnableAutoConfiguration
    class Config {

    }

    data class Sample( val column1: String )

}