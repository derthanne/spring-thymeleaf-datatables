package de.mthsoft.datatables.web

import de.mthsoft.datatables.model.DataTableColumnDefinition
import de.mthsoft.datatables.model.DataTableDefinition
import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.context.support.StaticMessageSource
import java.util.*

class MessageSourceDataTableTranslationProviderTest {

    @Test
    fun testDefault() {
        val value = "value";
        val key = "key";
        val fail = "fail";
        val messageSource = StaticMessageSource();
        messageSource.addMessage( key, Locale.getDefault(), value );

        val table = Mockito.mock( DataTableDefinition::class.java );
        val failColumn = Mockito.mock( DataTableColumnDefinition::class.java );
        Mockito.`when`( failColumn.columnName ).thenReturn( fail );
        val okColumn = Mockito.mock( DataTableColumnDefinition::class.java );
        Mockito.`when`( okColumn.columnName ).thenReturn( key );

        val provider = object : MessageSourceDataTableTranslationProvider( messageSource ) {
            override val infoKey: String
                get() = key;
            override val searchKey: String
                get() = fail;

            override fun getKeyForColumnLabel(
                table: DataTableDefinition<*>,
                column: DataTableColumnDefinition<*>
            ) : String {
                return if( column.columnName == fail ) {
                    fail;
                } else {
                    key;
                }
            }

        };
        Assertions.assertEquals( MessageSourceDataTableTranslationProvider.FIRST, provider.paginateFirst( Locale.getDefault() ) );
        Assertions.assertEquals( MessageSourceDataTableTranslationProvider.PREV, provider.paginatePrevious( Locale.getDefault() ) );
        Assertions.assertEquals( MessageSourceDataTableTranslationProvider.NEXT, provider.paginateNext( Locale.getDefault() ) );
        Assertions.assertEquals( MessageSourceDataTableTranslationProvider.LAST, provider.paginateLast( Locale.getDefault() ) );
        Assertions.assertEquals( value, provider.info( Locale.getDefault() ) );
        Assertions.assertEquals( StringUtils.EMPTY, provider.search( Locale.getDefault() ) );
        Assertions.assertEquals( value, provider.columnLabel( table, okColumn, Locale.getDefault() ) );
        Assertions.assertEquals( StringUtils.EMPTY, provider.columnLabel( table, failColumn, Locale.getDefault() ) );
    }

}