package de.mthsoft.datatables.model

import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.data.domain.PageImpl

class DataTablePageTest {

    @Test
    fun testEmpty() {
        val page = DataTablePage.empty<MockModel>();
        Assertions.assertEquals( 0, page.filteredElements );
        Assertions.assertEquals( 0, page.totalElements );
        Assertions.assertEquals( 1, page.totalPages );
        Assertions.assertTrue( page.isEmpty );
        Assertions.assertFalse( page.hasContent() );
        Assertions.assertTrue( page.content.isEmpty() );
        Assertions.assertNotNull( page.pageable );
    }

    @Test
    fun testFromList() {
        val list = listOf( MockModel(), MockModel() );
        val page = DataTablePage.fromList( list );
        Assertions.assertEquals( 2, page.filteredElements );
        Assertions.assertEquals( 2, page.totalElements );
        Assertions.assertEquals( 1, page.totalPages );
        Assertions.assertFalse( page.isEmpty );
        Assertions.assertTrue( page.hasContent() );
        Assertions.assertEquals( 2, page.content.size );
        Assertions.assertNotNull( page.pageable );
    }

    @Test
    fun testFromPage() {
        val list = listOf( MockModel(), MockModel() );
        val p = PageImpl( list );
        val page = DataTablePage.build( p, 2, 4 );
        Assertions.assertEquals( 2, page.filteredElements );
        Assertions.assertEquals( 4, page.totalElements );
        Assertions.assertEquals( 2, page.totalPages );
        Assertions.assertFalse( page.isEmpty );
        Assertions.assertTrue( page.hasContent() );
        Assertions.assertEquals( 2, page.content.size );
        Assertions.assertNotNull( page.pageable );
    }

}

data class MockModel( val property : String = StringUtils.EMPTY ) {



}