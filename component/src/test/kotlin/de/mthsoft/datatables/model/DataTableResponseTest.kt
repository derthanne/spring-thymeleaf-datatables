package de.mthsoft.datatables.model

import de.mthsoft.datatables.web.DataTableRequest
import de.mthsoft.datatables.web.DataTableResponse
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito

class DataTableResponseTest {

    @Test
    fun testFromException() {
        val request = Mockito.mock( DataTableRequest::class.java ) as DataTableRequest<*>;
        Mockito.`when`( request.draw ).thenReturn( -1 );

        val response = DataTableResponse.fromException( request, Exception("Error"));
        Assertions.assertEquals( -1, response.draw );
        Assertions.assertEquals( "Error", response.error );
        Assertions.assertEquals( 0, response.recordsTotal );
        Assertions.assertEquals( 0, response.recordsFiltered );
        Assertions.assertEquals( emptyList<Any>(), response.data );
    }

}