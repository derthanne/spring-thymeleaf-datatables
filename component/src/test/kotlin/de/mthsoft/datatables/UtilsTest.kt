package de.mthsoft.datatables

import org.apache.commons.lang3.StringUtils
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.io.IOException
import java.io.Serializable

class UtilsTest {

    @Test
    fun when_PropertyIsWritable_GetPropertyWriterReturnsMethod() {
        Assertions.assertNotNull( getPropertyWriter( FullAccess::class.java, "property" ));
        Assertions.assertNotNull( getPropertyWriter( WriteOnly::class.java, "property" ));
    }

    @Test
    fun when_PropertyIsNotWritable_GetPropertyWriterThrowsException() {
        Assertions.assertThrows( IllegalArgumentException::class.java) {
            getPropertyWriter(ReadOnly::class.java, "property");
        };
        Assertions.assertThrows( IllegalArgumentException::class.java) {
            getPropertyWriter(FullAccess::class.java, "prop");
        };
    }

    @Test
    fun when_PropertyIsReadable_GetPropertyReaderReturnsMethod() {
        Assertions.assertNotNull( getPropertyReader( FullAccess::class.java, "property" ));
        Assertions.assertNotNull( getPropertyReader( ReadOnly::class.java, "property" ));
    }

    @Test
    fun when_PropertyIsNotReadable_GetPropertyReaderThrowsException() {
        Assertions.assertThrows( IllegalArgumentException::class.java) {
            getPropertyReader(WriteOnly::class.java, "property");
        };
        Assertions.assertThrows( IllegalArgumentException::class.java) {
            getPropertyReader(FullAccess::class.java, "prop");
        };
    }

    @Test
    fun when_PropertyIsReadable_HasPropertyReturnsTrue() {
        Assertions.assertTrue( hasProperty( FullAccess::class.java, "property" ));
        Assertions.assertTrue( hasProperty( ReadOnly::class.java, "property" ));
    }

    @Test
    fun when_PropertyIsNotReadable_HasPropertyReturnsFalse() {
        Assertions.assertFalse( hasProperty( WriteOnly::class.java, "property" ));
    }

    @Test
    fun when_PropertyDoesNotExist_HasPropertyReturnsFalse() {
        Assertions.assertFalse( hasProperty( FullAccess::class.java, "prop" ));
    }

    @Test
    fun when_ObjectIsSerializable_DeepCopyReturnsCopy() {
        val list = ArrayList<String>();
        list.add( StringUtils.EMPTY );
        Assertions.assertEquals( list, deepCopy( list ) );

        Assertions.assertEquals( FullAccess(), deepCopy( FullAccess() ) );

    }

}

data class FullAccess( var property : String = StringUtils.EMPTY ) : Serializable {

}

class ReadOnly {

    val property : String = StringUtils.EMPTY;

}

class WriteOnly {

    private fun getProperty() = StringUtils.EMPTY;

    public fun setProperty( value : String ) {

    }

}