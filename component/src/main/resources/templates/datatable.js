let [[${varName}]];
$(function () {
    [[${varName}]] = $('[[${elementId}]]').DataTable({
        "responsive": [[${responsive}]],
        "ajax": { "url": "[[${url}]]", "type": "POST" },
        [# th:each="option : ${options}"]"[[${option.key}]]": [[${option.value}]], [/]
        "select": { "style": "api" }, "search": { "caseInsensitive": false },
        "columns": [ [[${columns}]] ],
        "rowId": "[[${idColumn}]]",
        "order": [ [ [[${sortColumn.index}]], '[[${sortColumn.order}]]' ] ],
        "language": { [[${language}]] }
    });
});