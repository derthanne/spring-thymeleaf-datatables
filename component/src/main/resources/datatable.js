let <varName>;
$(function () {
    <varName> = $('#<elementId>').DataTable({
        "serverSide": true,
        "responsive": <options.responsive>,
        "searchDelay": <options.searchDelay>,
        "paging": <options.paging>,
        "pagingType": "<options.pagingType>",
        "searching": <options.searching>,
        "ordering": <options.ordering>,
        "info": <options.info>,
        "autoWidth": true,
        "processing": <options.processing>,
        "lengthChange": <options.lengthChange>,
        "lengthMenu": [ 10, 20, 30, 40, 50 ],
        "order": [ [ <defaultSortColumnIndex>, '<defaultSortColumnOrder>' ] ],
        "ajax": {
            "url": "<url>",
            "type": "POST"
        },
        "rowId": "<idColumn>",
        "columns": [ <columnContent> ],
        "language": { <languageContent> }
    });

    $('#<elementId> tbody').on('click', 'td.<detailsControlClassName>', function() {
        let tr = $(this).closest('tr');
        let row = <varName>.row(tr);

        if( row.child.isShown() ) {
            // This row is already open - close it
            destroy<varName>Child(row, tr);
            tr.removeClass('shown');
        } else {
            // Open this row
            create<varName>Child(row, tr);
            tr.addClass('shown');
        }
    } );

    function create<varName>Child( row, tr ) {
        // This is the table we'll convert into a DataTable
        let table = $('<table class="display" width="100%"/>');

        // Display it the child row
        row.child( table ).show();

        // Initialise as a DataTable
        let <childVarName> = table.DataTable( {
            "serverSide": true,
            "searching": false,
            "ordering": false,
            "paging": false,
            "info": false,
            "autoWidth": true,
            "processing": false,
            "lengthChange": false,
            "select": {
                "style": "api"
            },
            "search": {
                "caseInsensitive": false
            },
            "ajax": {
                "url": "<detailsControlUrl>" + tr.attr('id'),
                "type": "POST"
            },
            "rowId": "<idColumn>",
            "columns": [ <childColumnContent> ]
        } );
    }

    function destroy<varName>Child(row, tr) {
        let table = $("table", row.child());
        table.detach();
        table.DataTable().destroy();

        // And then hide the row
        row.child.hide();
    }
});