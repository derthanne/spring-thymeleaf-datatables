package de.mthsoft.datatables.model

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable

/**
 * @author Marcel Thannhäuser
 */
interface DataTablePage<Type> : Page<Type> {

    companion object {

        @JvmStatic
        fun <Type> empty() : DataTablePage<Type> =
            DataTablePageImpl( Page.empty(), 0, 0 );

        @JvmStatic
        fun <Type> fromList( list : List<Type> ) : DataTablePage<Type> =
            DataTablePageImpl( list, Pageable.unpaged(), list.size.toLong(), list.size.toLong() );

        @JvmStatic
        fun <Type> build( page : Page<Type>, filteredElements: Long, total: Long ) : DataTablePage<Type> =
            DataTablePageImpl( page, filteredElements, total );

    }
    /**
     * The number of elements after filtering.
     * See [server side processing](https://datatables.net/manual/server-side) for details.
     */
    val filteredElements : Long;

}

private class DataTablePageImpl<Type>(content : List<Type>, pageable : Pageable, override val filteredElements: Long, total : Long )
    : PageImpl<Type>( content, pageable, total), DataTablePage<Type> {

    constructor(page: Page<Type>, filteredElements: Long, total: Long ) :
            this( page.content, page.pageable, filteredElements, total );

}