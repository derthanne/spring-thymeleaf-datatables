package de.mthsoft.datatables.model

import de.mthsoft.datatables.hasProperty
import org.slf4j.LoggerFactory
import java.beans.IntrospectionException
import kotlin.jvm.Throws

/**
 * @author Marcel Thannhäuser
 */
enum class SortDirection {

    ASC, DESC;

    override fun toString(): String {
        return name.lowercase();
    }

}

/**
 * This class define a jQuery datatable column and provides the config options that are useful for server side tables.
 * See [Options](https://datatables.net/reference/option/) for more details
 * Note that not all options are implemented at present.
 *
 * The following must be assured when constructing the DataTableColumnDefinition:
 * 1. The given type must contain a property that is named after the columnName property.
 * 2. TODO
 *
 * @author Marcel Thannhäuser
 */
class DataTableColumnDefinition<Type> @Throws( IllegalArgumentException::class, IntrospectionException::class ) private constructor (
    val type : Class<Type>,
    val identifier : Boolean = false,
    val columnName: String,
    @get:JvmName("isSearchable") val searchable: Boolean = false,
    /*
     * Named orderable in jQuery DataTables
     */
    @get:JvmName("isSortable") val sortable: Boolean = false,

    @get:JvmName("isVisible") val visible: Boolean = true,
    /**
     * Defines the optional width in percent. % will be added on rendering
     */
    val width: Int? = null,
    /**
     * Defines an optional render function body
     * signature: function ( data, type, row, meta )
     * See (https://datatables.net/reference/option/columns.render#Types)
     */
    val render: String? = null,
    @get:JvmName("isDefaultSortColumn") val defaultSortColumn : Boolean = false,
    val defaultSortDirection : SortDirection = SortDirection.ASC,
    val childTable : DataTableDefinition<*>? = null
) {

    init {
        if( !hasProperty( type, columnName ) ) {
            throw IllegalArgumentException( "No property named $columnName found for type ${type.name}");
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DataTableColumnDefinition<*>

        if (type != other.type) return false
        if (columnName != other.columnName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = type.hashCode()
        result = 31 * result + columnName.hashCode()
        return result
    }

    override fun toString(): String {
        return "DataTableColumnDefinition(columnName='$columnName', searchable=$searchable, sortable=$sortable, identifier=$identifier)"
    }

    /**
     * Builder object for a DataTableColumnDefinition
     */
    data class Builder<Type> (
        private val table: DataTableDefinition.Builder<Type>,
        private val type : Class<Type>,
        private val columnName: String
    ) {
        private val logger = LoggerFactory.getLogger( Builder::class.java );

        private var identifier : Boolean = false;
        private var searchable: Boolean = false;
        private var visible: Boolean = true;
        private var sortable: Boolean = false;
        private var width: Int? = null;
        private var render: String? = null;
        private var defaultSortColumn : Boolean = false;
        private var defaultSortDirection : SortDirection = SortDirection.ASC;
        private var child : DataTableDefinition<*>? = null;

        /**
         * Configure this column as identifier. Note that this cannot be reset.
         */
        fun asIdentifier() = apply { this.identifier = true; }

        /**
         * Configure this column as searchable. Note that this cannot be reset.
         */
        fun asSearchable() = apply { this.searchable = true; }

        /**
         * Configure this column as invisible. Note that this cannot be reset.
         */
        fun asInvisible() = apply { this.visible = false; }

        /**
         * Configure this column as sortable (orderable). Note that this cannot be reset.
         */
        fun asSortable() = apply { this.sortable = true; }

        /**
         * Configure this column as default sort column. Note that this cannot be reset.
         * This also sets the column to orderable.
         */
        fun asDefaultSortColumn() = apply {
            asSortable();
            this.defaultSortColumn = true;
        }

        /**
         * Configure the default sort direction of this column. Note that this cannot be reset.
         * This also sets the column to orderable and as the default sort column.
         */
        fun withDefaultSortDirection( defaultSortDirection: SortDirection ) = apply {
            asDefaultSortColumn();
            this.defaultSortDirection = defaultSortDirection;
        }

        fun withWidth( width: Int ) = apply { this.width = width; }

        fun withRender( render: String ) = apply { this.render = render; }

        fun <SubType> withChildTable( child: DataTableDefinition<SubType> ) = apply { this.child = child }

        /**
         * Ends this column and returns the table definition for fluent builder style.
         */
        fun endColumn() = table;

        @Throws( IllegalArgumentException::class, IntrospectionException::class )
        fun build() : DataTableColumnDefinition<Type> {
            val def = DataTableColumnDefinition( type,
                identifier,
                columnName,
                searchable,
                sortable,
                visible,
                width,
                render,
                defaultSortColumn,
                defaultSortDirection,
                child);
            if( logger.isInfoEnabled ) {
                logger.info( "Built {}", def );
            }
            return def;
        }

    }

}

/**
 * This class define a jQuery datatable and provides the config options that are useful for server side tables.
 * See [Options](https://datatables.net/reference/option/) for more details
 * Note that not all options are implemented at present.
 *
 * The following must be assured when constructing the DataTableDefinition:
 * 1. There must be at least one column in the array of DataTableColumnDefinition objects.
 * 1. At least one column must be defined as identifier.
 * 1. At least one column must be defined as default sort column.
 *
 * @author Marcel Thannhäuser
 */
class DataTableDefinition<Type> @Throws( IllegalArgumentException::class, IntrospectionException::class ) private constructor (
    val name: String,
    val url: String,
    val features: DataTableFeatures,
    val options: DataTableOptions,
    val columns: Array<DataTableColumnDefinition<Type>>
) {

    init {
        if( columns.isEmpty() ) throw IllegalArgumentException( "A Datatable needs at least one column!" );
        columns.find { it.identifier }?: throw IllegalArgumentException( "A Datatable needs an identifier column!" );
        columns.find { it.defaultSortColumn }?: throw IllegalArgumentException( "A Datatable needs a default sort column!" );
    }

    val idColumn: DataTableColumnDefinition<Type> get() = columns.find { it.identifier }!!;

    val defaultSortColumn : DataTableColumnDefinition<Type> get() = columns.find { it.defaultSortColumn }!!;

    fun getColumnByName( name: String ) : DataTableColumnDefinition<Type>? = columns.find { it.columnName == name };

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DataTableDefinition<*>

        if (features != other.features) return false
        if (options != other.options) return false
        if (!columns.contentEquals(other.columns)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = features.hashCode()
        result = 31 * result + options.hashCode()
        result = 31 * result + columns.contentHashCode()
        return result
    }

    override fun toString(): String {
        return "DataTableDefinition(columns=${columns.contentToString()}, idColumn=${idColumn.columnName}, defaultSortColumn=${defaultSortColumn.columnName})"
    }

    /**
     * Builder object for a DataTableDefinition
     */
    data class Builder<Type>(
        private val name: String,
        private val type : Class<Type>,
        private val url: String
    ) {

        private val logger = LoggerFactory.getLogger( Builder::class.java );

        private var features: DataTableFeatures = DataTableFeatures.withDefaults();
        private var options: DataTableOptions = DataTableOptions.withDefaults();

        private val columns = LinkedHashMap<String,DataTableColumnDefinition.Builder<Type>>();

        /**
         * Sets the features of the resulting DataTableComponent.
         * Existing features will be overwritten.
         */
        fun withFeatures( features: DataTableFeatures ) = apply { this.features = features }

        /**
         * Sets the options of the resulting DataTableComponent.
         * Existing options will be overwritten.
         */
        fun withOptions( options: DataTableOptions ) = apply { this.options = options }

        /**
         * Creates a new builder for a DataTableColumnDefinition with the given column name.
         * Note that a column must be related to a Java property of the same name.
         * @exception IllegalStateException if the given columnName already exists in this table.
         */
        @Throws( IllegalStateException::class )
        fun addColumn( columnName: String ) : DataTableColumnDefinition.Builder<Type> {
            if( columns.containsKey( columnName ) ) {
                throw IllegalStateException( "Column $columnName has already been defined!" );
            }
            val builder = DataTableColumnDefinition.Builder( this, type, columnName );
            columns[columnName] = builder;
            return builder;
        }

        @Throws( IllegalArgumentException::class, IntrospectionException::class )
        fun build() : DataTableDefinition<Type> {
            val cols = this.columns.values.map { it.build() }.toTypedArray();
            val def = DataTableDefinition( name, url, features, options, cols );
            if( logger.isInfoEnabled ) {
                logger.info( "Built {}", def );
            }
            return def;
        }

    }

}