package de.mthsoft.datatables.model

/**
 * As described in [Features](https://datatables.net/reference/option).
 * Note that not all options are implemented at present.
 *
 * @author Marcel Thannhäuser
 */
//Defaults for those:
//val serverSide : Boolean = true,
class DataTableFeatures private constructor(
    @get:JvmName("hasSearching") var searching: Boolean = true,
    @get:JvmName("hasPaging") var paging : Boolean = true,
    @get:JvmName("hasOrdering") var ordering: Boolean = true,
    @get:JvmName("hasInfo") var info: Boolean = true,
    @get:JvmName("hasProcessing") var processing: Boolean = false,
    @get:JvmName("hasLengthChange") var lengthChange: Boolean = false,
    @get:JvmName( "isResponsive" ) var responsive: Boolean = true
) {

    companion object {

        @JvmStatic
        fun withAllDisabled() = DataTableFeatures( false, false, false, false, false, false, false );

        @JvmStatic
        fun withAllEnabled() = DataTableFeatures( true, true, true, true, true, true, true );

        @JvmStatic
        fun withDefaults() = DataTableFeatures();

    }

}

/**
 * As described in [Options](https://datatables.net/reference/option).
 * Note that not all options are implemented at present.
 *
 * @author Marcel Thannhäuser
 */
//Defaults for those:
//val search.caseInsensitive : Boolean = false
class DataTableOptions private constructor(
    var pagingType : PagingType = PagingType.SIMPLE,
    var searchDelay : Int = 500
) {

    companion object {

        @JvmStatic
        fun withDefaults() = DataTableOptions();

        @JvmStatic
        fun withPagingType( pagingType: PagingType ) = DataTableOptions( pagingType );

    }

    enum class PagingType {

        NUMBERS,
        SIMPLE,
        SIMPLE_NUMBERS,
        FULL,
        FULL_NUMBERS,
        FIRST_LAST_NUMBERS;

        override fun toString(): String {
            return name.lowercase();
        }

    }

}