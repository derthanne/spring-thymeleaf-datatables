@file:JvmName("Utils")
package de.mthsoft.datatables

import org.apache.commons.beanutils.DefaultBeanIntrospector
import org.apache.commons.beanutils.IntrospectionContext
import org.slf4j.LoggerFactory
import java.beans.IntrospectionException
import java.beans.PropertyDescriptor
import java.io.*
import java.lang.reflect.Method
import java.util.*
import kotlin.jvm.Throws

/**
 * Get a method that grants write access to the given property of the given class
 *
 * @author Marcel Thannhäuser
 *
 * @throws IllegalArgumentException if the property or method cannot be found
 */
@Throws( IllegalArgumentException::class, IntrospectionException::class )
fun getPropertyWriter(clazz: Class<*>, propertyName: String ) : Method {
    val prop = Introspector.getProperty( clazz, propertyName );
    return prop?.writeMethod ?: throw IllegalArgumentException( "Property $propertyName is nonexistent, invisible or not writeable for class ${clazz.name}!");
}

/**
 * Get a method that grants read access to the given property of the given class
 *
 * @author Marcel Thannhäuser
 *
 * @throws IllegalArgumentException if the property or method cannot be found
 */
@Throws( IllegalArgumentException::class, IntrospectionException::class )
fun getPropertyReader(clazz: Class<*>, propertyName: String ) : Method {
    val prop = Introspector.getProperty( clazz, propertyName );
    return prop?.readMethod ?: throw IllegalArgumentException( "Property $propertyName is nonexistent, invisible or not writeable for class ${clazz.name}!");
}

/**
 * Checks if the given class contains a property with the given name.
 * The method also checks if the property is readable.
 *
 * @author Marcel Thannhäuser
 */
@Throws( IllegalArgumentException::class, IntrospectionException::class )
fun hasProperty(clazz: Class<*>, propertyName: String ) : Boolean {
    val logger = LoggerFactory.getLogger( Introspector::class.java );
    if( !Introspector.hasProperty( clazz, propertyName ) ) {
        if( logger.isDebugEnabled ) {
            logger.debug( "Property {} unknown for class {}", propertyName, clazz.name );
        }
        return false;
    }
    return try {
        getPropertyReader(clazz, propertyName);
        if( logger.isDebugEnabled ) {
            logger.debug( "Property {} readable for class {}", propertyName, clazz.name );
        }
        true;
    } catch ( e : IllegalArgumentException ) {
        if( logger.isDebugEnabled ) {
            logger.debug( "Property {} not readable for class {}", propertyName, clazz.name );
        }
        false;
    }
}

/**
 * @author Marcel Thannhäuser
 */
private object Introspector {

    private val contextes = Collections.synchronizedSet( mutableSetOf<IntrospectionContext>() );

    @Throws( IntrospectionException::class )
    private fun getIntrospectionContext(clazz: Class<*> ) : IntrospectionContext {
        return contextes.find { it.targetClass == clazz } ?: buildIntrospectionContext( clazz );
    }

    @Throws( IntrospectionException::class )
    private fun buildIntrospectionContext(clazz: Class<*> ) : IntrospectionContext {
        val context = IntrospectionContextImpl( clazz );
        synchronized( contextes ) {
            DefaultBeanIntrospector.INSTANCE.introspect( context );
            this.contextes.add( context );
        }
        return context;
    }

    @Throws( IntrospectionException::class )
    fun getProperty( clazz: Class<*>, propertyName: String ) : PropertyDescriptor? =
        getIntrospectionContext( clazz ).getPropertyDescriptor( propertyName );

    @Throws( IntrospectionException::class )
    fun hasProperty(clazz: Class<*>, propertyName: String ) =
        getIntrospectionContext( clazz ).hasProperty( propertyName );

    private class IntrospectionContextImpl( private val clazz: Class<*> ) : IntrospectionContext {

        private val logger = LoggerFactory.getLogger( IntrospectionContextImpl::class.java );

        private val props = mutableListOf<PropertyDescriptor>();

        override fun getTargetClass() = clazz;

        override fun addPropertyDescriptor(desc: PropertyDescriptor) {
            try {
                desc.readMethod?.isAccessible = true
            } catch ( e : Exception ) {
                logger.warn( "Could not make read method of property {} accessible. This may lead to further errors!", desc.name );
            }
            try {
                desc.writeMethod?.isAccessible = true
            } catch ( e : Exception ) {
                logger.warn( "Could not make write method of property {} accessible. This may lead to further errors!", desc.name );
            }
            props.add( desc );
        }

        override fun addPropertyDescriptors(descriptors: Array<out PropertyDescriptor>) {
            descriptors.forEach { addPropertyDescriptor( it ); }
        }

        override fun hasProperty(name: String): Boolean {
            return props.find { it.name == name } != null
        }

        override fun getPropertyDescriptor(name: String): PropertyDescriptor? {
            return props.find { it.name == name }
        }

        override fun removePropertyDescriptor(name: String) {
            val prop = getPropertyDescriptor( name );
            if( null != prop ) {
                props.remove( prop );
            }
        }

        override fun propertyNames(): Set<String> {
            return props.map { it.name }.toSet();
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            if (clazz != (other as IntrospectionContextImpl).clazz) return false

            return true
        }

        override fun hashCode(): Int {
            return clazz.hashCode()
        }

    }

}

/**
 * Generate a deep copy of the given object.
 * The copy is created via Java serialization and deserialization.
 * Note: content will be cached in a byte array in memory, so take care of available memory!
 *
 * @author Marcel Thannhäuser
 */
@Throws( IOException::class )
fun <Type: Serializable> deepCopy(`object` : Type )  = CopyHelper( `object` ).createCopy();

/**
 * @author Marcel Thannhäuser
 */
private class CopyHelper<Type : Serializable> @Throws( IOException::class ) constructor(obj: Type ) {

    private val content = ByteArrayOutputStream();

    init {
        val oos = ObjectOutputStream(content)
        oos.use {
            it.writeObject( obj );
        }
    }

    @Throws( IOException::class )
    fun createCopy() : Type {
        @Suppress("unchecked_cast")
        return ObjectInputStream(ByteArrayInputStream(content.toByteArray())).readObject() as Type
    }

}