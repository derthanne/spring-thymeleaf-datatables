package de.mthsoft.datatables.web

import com.fasterxml.jackson.annotation.JsonInclude
import de.mthsoft.datatables.model.DataTablePage
import org.apache.commons.lang3.RandomUtils
import org.springframework.data.domain.Page

/**
 * A response that is compliant to the [server side processing](https://datatables.net/manual/server-side) specification of jquery data tables.
 * Can be used in controllers or elsewhere.
 * Note that not all features are implemented at present.
 *
 * @author Marcel Thannhäuser
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
class DataTableResponse<Type> private constructor(request : DataTableRequest<Type>?,
                                                  page : DataTablePage<Type>?,
                                                  exception: Exception? = null) {

    companion object {

        @JvmStatic
        fun <Type> fromException(request : DataTableRequest<Type>, exception: Exception ) =
            DataTableResponse.Builder<Type>().withRequest( request ).withException( exception ).build();

        @JvmStatic
        fun <Type> fromDataTablePage(request: DataTableRequest<Type>, page: DataTablePage<Type> ) =
            DataTableResponse.Builder<Type>().withRequest( request ).withPage( page ).build();

        @JvmStatic
        fun <Type> fromPage(request: DataTableRequest<Type>, page: Page<Type> ) =
            DataTableResponse.Builder<Type>().withRequest( request ).withPage( DataTablePage.build( page, page.totalElements, page.totalElements ) ).build();

        @JvmStatic
        fun <Type> fromList(request: DataTableRequest<Type>, list: List<Type> ) =
            DataTableResponse.Builder<Type>().withRequest( request ).withPage( DataTablePage.fromList( list ) ).build();

    }

    val draw : Int = request?.draw ?: RandomUtils.nextInt();

    val data : List<Type> = if (page?.hasContent() == true) {
        page.content;
    } else {
        emptyList();
    };

    val recordsTotal : Long = page?.totalElements ?: 0;

    val recordsFiltered : Long = page?.filteredElements ?: 0;

    val error: String?  = exception?.message;

    private class Builder<Type>() {

        private var request : DataTableRequest<Type>? = null;

        private var page : DataTablePage<Type>? = null;

        private var exception: Exception? = null;

        fun withRequest( request: DataTableRequest<Type> ) = apply {
            this.request = request;
        }

        fun withPage( page: DataTablePage<Type> ) = apply {
            this.page = page;
        }

        fun withException( exception: Exception ) = apply {
            this.exception = exception;
        }

        fun build() : DataTableResponse<Type> {
            //Will only happen if this class becomes public
            if( null == page && null == exception ) {
                throw IllegalStateException("Either a result OR an exception must be set!" );
            }
            //Will only happen if this class becomes public
            if( null != page && null != exception ) {
                throw IllegalStateException("Either a result OR an exception must be set, not both!" );
            }
            return DataTableResponse( request, page, exception );
        }

    }

}

