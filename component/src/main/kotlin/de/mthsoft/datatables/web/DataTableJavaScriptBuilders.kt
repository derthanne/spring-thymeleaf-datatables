package de.mthsoft.datatables.web

import de.mthsoft.datatables.model.DataTableColumnDefinition
import de.mthsoft.datatables.model.DataTableDefinition
import org.apache.commons.lang3.StringUtils
import org.springframework.core.io.ClassPathResource
import org.thymeleaf.TemplateSpec
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.SpringTemplateEngine
import org.thymeleaf.templatemode.TemplateMode

/**
 * @author Marcel Thannhäuser
 */
interface DataTableJavaScriptBuilder {

    companion object {
        fun create( component: DataTableComponent<*>, templateEngine: SpringTemplateEngine? = null ) : DataTableJavaScriptBuilder {
            return if ( null != templateEngine ) {
                ThymeleafBuilder( component, templateEngine );
            } else {
                CustomBuilder( component );
            }
        }
    }

    fun build() : String;

}

/**
 * @author Marcel Thannhäuser
 */
internal abstract class AbstractDataTableJavaScriptBuilder(protected val component: DataTableComponent<*> ) : DataTableJavaScriptBuilder {

    //TODO: Make this configurable
    protected val detailsControlClassName = "details-control";

    protected fun buildLanguageJSONObjectContent() : String {
        val translationProvider = component.translationProvider;
        val locale = component.locale;
        val json = StringBuilder(50);
        translationProvider.info( locale )?.let {
            json.append( "'info': '$it', ")
        }
        translationProvider.search( locale )?.let {
            json.append( "'search': '$it', ")
        }
        val paginate = StringBuilder( 30);
        mapOf(
            "first" to translationProvider.paginateFirst( locale ),
            "previous" to translationProvider.paginatePrevious( locale ),
            "next" to translationProvider.paginateNext( locale ),
            "last" to translationProvider.paginateLast( locale )
        ).forEach { (key, value) ->
            value?.let {
                if(paginate.isNotEmpty()) {
                    paginate.append( ',' );
                }
                paginate.append( "'$key': '$value'");
            }
        }
        json.append( "'paginate': {").append( paginate ).append( "}");
        return json.toString();
    }

    protected fun buildColumnDefinitionsJSONArrayContent( definition : DataTableDefinition<*> ) : String {
        val columns = StringBuilder( definition.columns.size * 50 );
        definition.columns.forEach {
            val column = StringBuilder( 50 );
            //Prepend new line
            if( columns.isNotEmpty() ) {
                column.append( ",\n" );
            }
            column.apply {
                append( '{' );
                //child table creates a special column
                if( null != it.childTable ) {
                    append( "'className': '");
                    append( detailsControlClassName.trim() );
                    append( "', 'orderable': false, 'searchable': false");
                    append( ", 'data': '");
                    append( it.columnName );
                    append( "'")
                } else {
                    //data property
                    append( "'data': '").append( it.columnName ).append( "'" );
                    //boolean properties. safe to set without check
                    append( ", 'orderable': ").append( it.sortable );
                    append( ", 'searchable': ").append( it.searchable );
                    append( ", 'visible': ").append( it.visible );
                }
                //for child tables add the title via javascript
                if( component.isChild( definition ) ) {
                    append( ", 'title': '");
                    append( getColumnLabel( definition, it ) );
                    append( "'");
                }
                //width will be written as %
                if( it.width != null ) {
                    append( ", 'width': '").append( it.width ).append( "%'" );
                }
                //now render function
                if( it.render != null ) {
                    append( ", 'render': function( data, type, row, meta) {\n")
                    append( it.render );
                    append( "\n}");
                }
                append( '}' );
            }
            columns.append( column.toString() );
        }
        return columns.toString();
    }

    protected fun getColumnLabel( definition: DataTableDefinition<*>, column : DataTableColumnDefinition<*> ) : String? {
        return component.translationProvider.columnLabel( definition, column, component.locale );
    }

    protected fun hasChildTable() = component.definition.columns.find { c -> c.childTable != null } != null;

    protected fun getChildTableDefinition() = component.definition.columns.find {  c -> c.childTable != null }?.childTable;

}

/**
 * @author Marcel Thannhäuser
 */
@Deprecated( "Support will be removed in a future version")
internal class ThymeleafBuilder(component: DataTableComponent<*>,
                                private val templateEngine: SpringTemplateEngine ) : AbstractDataTableJavaScriptBuilder( component ) {

    companion object {

        private val TEMPLATE = TemplateSpec( "datatable.js", TemplateMode.TEXT );

    }

    override fun build() : String {
        val definition = component.definition;
        val options = mapOf(
            "serverSide" to true,
            "autoWidth" to true,
            "searchDelay" to definition.options.searchDelay,
            "pagingType" to "'" + definition.options.pagingType.toString() + "'",
            "paging" to definition.features.paging,
            "searching" to definition.features.searching,
            "ordering" to definition.features.ordering,
            "info" to definition.features.info,
            "processing" to definition.features.processing,
            "lengthChange" to definition.features.lengthChange
        );
        val sortColumn = mapOf(
            "index" to definition.columns.indexOf( component.definition.defaultSortColumn ).toString(),
            "order" to definition.defaultSortColumn.defaultSortDirection.toString(),
        );
        val variables = mapOf(
            "url" to component.definition.url,
            "elementId" to "#" + component.definition.name,
            "varName" to component.definition.name,
            "idColumn" to definition.idColumn.columnName,
            "sortColumn" to sortColumn,
            "options" to options,
            "responsive" to component.definition.features.responsive,
            "columns" to buildColumnDefinitionsJSONArrayContent( this.component.definition ),
            "language" to buildLanguageJSONObjectContent()
        );
        val context = Context( component.locale, variables );
        var rendered = templateEngine.process( TEMPLATE, context ).trim();
        rendered = StringUtils.replace( rendered, "&#39;", "'" );
        return StringUtils.replace( rendered, "&quot;", "\"");
    }

}

/**
 * @author Marcel Thannhäuser
 */
internal class CustomBuilder(component: DataTableComponent<*>) : AbstractDataTableJavaScriptBuilder( component ) {

    companion object {

        private val TEMPLATE = ClassPathResource( "/datatable.js" ).inputStream.readBytes();

    }

    override fun build() : String {
        val definition = component.definition;
        var detailsControlClassName : String? = null;
        var detailsControlUrl : String? = null;
        var detailsControlColumns : String? = null;
        var detailsTableVarName : String = StringUtils.EMPTY;
        getChildTableDefinition()?.let {
            detailsControlClassName = this.detailsControlClassName;
            detailsControlUrl = it.url;
            detailsControlColumns = buildColumnDefinitionsJSONArrayContent( it )
            detailsTableVarName = it.name;
        }
        val replacements = mapOf(
            "url" to component.definition.url,
            "elementId" to component.definition.name,
            "varName" to "dt${component.definition.name}",
            "childVarName" to "dt$detailsTableVarName",
            "columnContent" to buildColumnDefinitionsJSONArrayContent( this.component.definition ),
            "childColumnContent" to detailsControlColumns,
            "languageContent" to buildLanguageJSONObjectContent(),
            "idColumn" to definition.idColumn.columnName,
            "detailsControlUrl" to detailsControlUrl,
            "detailsControlClassName" to detailsControlClassName,
            "defaultSortColumnIndex" to definition.columns.indexOf( component.definition.defaultSortColumn ).toString(),
            "defaultSortColumnOrder" to definition.defaultSortColumn.defaultSortDirection.toString(),
            "options.searchDelay" to definition.options.searchDelay.toString(),
            "options.pagingType" to definition.options.pagingType.toString(),
            "options.paging" to definition.features.paging.toString(),
            "options.searching" to definition.features.searching.toString(),
            "options.ordering" to definition.features.ordering.toString(),
            "options.info" to definition.features.info.toString(),
            "options.processing" to definition.features.processing.toString(),
            "options.lengthChange" to definition.features.lengthChange.toString(),
            "options.responsive" to definition.features.responsive.toString()
        );
        var tmpl = String( TEMPLATE );
        replacements.forEach { (key, value) -> run {
            value?.apply {
                tmpl = tmpl.replace("<$key>", this, false)
            }
        } }
        return tmpl;
    }

}