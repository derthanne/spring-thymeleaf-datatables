package de.mthsoft.datatables.web

import de.mthsoft.datatables.model.DataTableColumnDefinition
import de.mthsoft.datatables.model.DataTableDefinition
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource
import org.springframework.context.NoSuchMessageException
import java.util.*

/**
 * Translations for the Datatable as described in [Language](https://datatables.net/reference/option/language).
 *
 * @author Marcel Thannhäuser
 */
interface DataTableTranslationProvider {

    fun info(locale: Locale ): String?;

    fun search(locale: Locale ): String?;

    fun paginateFirst(locale: Locale ): String?;

    fun paginatePrevious(locale: Locale ): String?;

    fun paginateNext(locale: Locale ): String?;

    fun paginateLast(locale: Locale ) : String?;

    fun columnLabel(table: DataTableDefinition<*>, column: DataTableColumnDefinition<*>, locale: Locale ) : String;

}

/**
 * DataTableTranslationProvider using a message source.
 * Provides useful defaults for some methods.
 *
 * @author Marcel Thannhäuser
 */
abstract class MessageSourceDataTableTranslationProvider(
    private val messageSource : MessageSource
) : DataTableTranslationProvider {

    private val logger = LoggerFactory.getLogger( MessageSourceDataTableTranslationProvider::class.java );

    companion object {

        const val FIRST = "«";

        const val PREV = "‹";

        const val NEXT = "›";

        const val LAST = "»";

    }

    protected abstract val infoKey: String?;

    protected abstract val searchKey: String?;

    protected abstract fun getKeyForColumnLabel(table: DataTableDefinition<*>, column: DataTableColumnDefinition<*> ) : String;

    override fun paginateFirst(locale: Locale): String = FIRST;

    override fun paginatePrevious(locale: Locale): String = PREV;

    override fun paginateNext(locale: Locale): String = NEXT;

    override fun paginateLast(locale: Locale): String = LAST;

    override fun columnLabel(table: DataTableDefinition<*>, column: DataTableColumnDefinition<*>, locale: Locale) =
        safeResolve( this.messageSource, getKeyForColumnLabel( table, column), locale, table, column );

    override fun info(locale: Locale) = infoKey?.let {
        safeResolve( it, locale );
    }

    override fun search(locale: Locale) = searchKey?.let {
        safeResolve( it, locale );
    }

    private fun safeResolve(key: String, locale: Locale ) : String {
        return safeResolve( this.messageSource, key, locale);
    }

    /**
     * Tries to resolve the given key from the given MessageSource.<br>
     * If a NoSuchMessageException is thrown this is caught and an empty String is returned.
     */
    protected fun safeResolve(messageSource: MessageSource, key: String, locale: Locale ) : String {
        return try {
            messageSource.getMessage( key, null, locale );
        } catch ( e : NoSuchMessageException ) {
            logger.warn( e.message );
            StringUtils.EMPTY;
        }
    }

    /**
     * Calls safeResolve without extra arguments
     */
    protected fun safeResolve(messageSource: MessageSource, key: String, locale: Locale, table: DataTableDefinition<*>, column: DataTableColumnDefinition<*> ) : String {
        return safeResolve( messageSource, key, locale);
    }
}