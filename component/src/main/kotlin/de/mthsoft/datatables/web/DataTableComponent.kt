package de.mthsoft.datatables.web

import de.mthsoft.datatables.model.DataTableColumnDefinition
import de.mthsoft.datatables.model.DataTableDefinition
import org.thymeleaf.spring5.SpringTemplateEngine
import java.util.*

/**
 * @author Marcel Thannhäuser
 */
class DataTableComponent<Type>(
    private val javaScriptBuilder: DataTableJavaScriptBuilder,
    val definition: DataTableDefinition<Type>,
    val translationProvider: DataTableTranslationProvider,
    val locale: Locale) {

    val columns : List<DataTableColumnDefinition<Type>>
        get() = definition.columns.toList()

    fun buildJavaScript() = javaScriptBuilder.build();

    fun getColumnLabel( column: DataTableColumnDefinition<*> ) =
        translationProvider.columnLabel( definition, column , locale )

    fun isChild( definition: DataTableDefinition<*> ) =
        this.definition.columns.find { definition == it.childTable } != null;

}