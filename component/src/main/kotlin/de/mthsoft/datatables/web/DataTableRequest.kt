package de.mthsoft.datatables.web

import de.mthsoft.datatables.model.DataTableColumnDefinition
import de.mthsoft.datatables.model.DataTableDefinition
import de.mthsoft.datatables.model.SortDirection
import org.springframework.util.MultiValueMap

/**
 * A request that is compliant to the [server side processing](https://datatables.net/manual/server-side) specification of jquery data tables.
 * Can be used in controllers or elsewhere.
 * Note that not all features are implemented at present.
 *
 * @author Marcel Thannhäuser
 */
class DataTableRequest<Type>( map: MultiValueMap<String,Any>, definition: DataTableDefinition<Type>) {

    private val order: MutableList<Order<Type>> = mutableListOf();

    private val cols: MutableList<Column<Type>> = mutableListOf();

    val draw: Int = map["draw"]!![0].toString().toInt();

    val start: Int = map["start"]!![0].toString().toInt();

    val length : Int = map["length"]!![0].toString().toInt();

    val searchValue: String = map["search[value]"]!![0].toString();

    init {
        var i = 0;
        while( true ) {
            map["columns[$i][data]"]?.let { names ->
                val searchable : Boolean = map["columns[$i][searchable]"]?.let {
                    "true" == it[0];
                }?: false;
                val col = definition.getColumnByName( names[0].toString() );
                cols.add( Column( col!!, searchable ) );
                i++;
            }?: break;
        }
        i = 0;
        while( true ) {
            map["order[$i][column]"]?.let {
                val dir = SortDirection.valueOf( map["order[$i][dir]"]!![0].toString().uppercase() );
                order.add( Order( cols[ it[0].toString().toInt() ], dir ) );
                i++;
            }?: break;
        }
    }

    //val columns get() = cols.toList();

    @get:JvmName("isSorted")
    val sorted get() = order.isNotEmpty();

    val sortColumn get() = if( order.size > 0 ) { order[0].name } else { null };

    val sortDirection get() = if( order.size > 0 ) { order[0].direction } else { null };

    private data class Column<Type>(val column : DataTableColumnDefinition<Type>, val searchable: Boolean ) {

    }

    private data class Order<Type>(val column : Column<Type>, val direction : SortDirection) {

        val name = column.column.columnName;

    }

}

