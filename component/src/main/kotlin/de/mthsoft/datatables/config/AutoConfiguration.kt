package de.mthsoft.datatables.config

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.http.CacheControl
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import java.util.concurrent.TimeUnit

/**
 * @author Marcel Thannhäuser
 */
@Configuration
@EnableWebMvc
class AutoConfiguration : WebMvcConfigurer  {

    private val logger = LoggerFactory.getLogger( AutoConfiguration::class.java );

    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        val cacheControlOneDay = CacheControl.maxAge(1, TimeUnit.DAYS);
        logger.info( "Adding ResourceHandler '/webjars/**' with Cache Control $cacheControlOneDay to resource location '/webjars/'");
        registry.addResourceHandler("/webjars/**").setCacheControl(cacheControlOneDay).addResourceLocations("/webjars/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("/webjars/").resourceChain(false);
        logger.info( "Adding ResourceHandler '/**' with Cache Control $cacheControlOneDay to resource location 'classpath:/static/'");
        registry.addResourceHandler("/**").setCacheControl(cacheControlOneDay).addResourceLocations("classpath:/static/");
    }

}